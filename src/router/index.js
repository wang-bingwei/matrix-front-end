import Vue from 'vue'
import Router from 'vue-router'


import login from '../components/Login.vue'
import index from '../components/Index.vue'
import noFound from '../components/404.vue'
import Test from '../components/Test.vue'

Vue.use(Router)

//登陆前的路由：
const commonRoutes = [{
		path: '/login',
		name: 'login',
		meta: {
			title: '登录'
		},
		component: login,
	},
	{
		path: '/index',
		name: 'index',
		meta: {
			title: '主页'
		},
		component: index,
	},
	// {
	//     path: '/other', // 点击侧边栏跳到一个单独的路由页面，需要定义，层级和其他顶级路由一样
	//     name: 'other',
	//     meta: { title: '单独的路由' },
	//     // component: () => import('../views/Other.vue'),
	// },
	{
		path: '/404',
		name: '404',
		meta: {
			title: '404'
		},
		component: noFound,
	},

	{
		path: '/Test',
		name: 'test',
		meta: {
			title: 'test'
		},
		component: Test,
	},
	{
		path: '/',
		redirect: 'login'
	},
]








//重置路由信息
export function resetRouter() {
	console.log("退出！重设路由")
	const newRouter = createRouter()
	router.matcher = newRouter.matcher
	
}


const createRouter = () => new Router({
	routes: commonRoutes,
})


const router = createRouter()
export default router

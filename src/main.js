import Vue from 'vue'
import Test from './components/Test.vue'

import router from './router/index.js'
import App from '@/App.vue'
import Login from './components/Login.vue'

Vue.config.productionTip = false

new Vue({
	router: router,
	render: h => h(App),
}).$mount('#app')

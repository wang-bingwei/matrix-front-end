# Matrix_前端

## Name and description
Four pockets

## project context
This warehouse is displayed for the front-end interface

## Requirements
The front-end uses a progressive framework for building user interface: vue.js

## Install
 Install the Node environment at: https://nodejs.org/en/
 npm install router vue-router vuex axios element-ui resource
 npm run serve 即可运行

## Main Project Leader

Wang Bingwei ， Wang Xiaoling

## relevant project
 .matrix（The back-end project）
## Participation and contribution mode
 Open source licenses

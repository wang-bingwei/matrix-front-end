# 名称和说明
四维口袋

## 项目介绍
    - 想要做出一款不限速、简洁、好用的云盘
    - 这个仓库为前端界面展示
## 运行环境
    - 前端采用构建用户界面的渐进式框架：vue.js
## 安装
    - 安装node环境：下载地址为：https://nodejs.org/en/
    - npm install router vue-router vuex axios element-ui resource
    - npm run serve 即可运行
## 主要项目负责人
    王冰炜，王晓玲

## 内置功能
登录
注册

## 技术框架
    VUE

## 相关项目
[.matrix（后端项目）](https://gitee.com/wang-bingwei/matrix)


// module.exports = {
// 	devServer: {
// 		Proxy:{
// 			'/api':{
// 				target:'http://192.168.3.17:8080/',
// 				changeOrigin: true,
// 				pathRewrite:{
// 					'^/api':'/'
// 				}
// 			}
// 		}
// 	}
// }

module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8080/',
        changeOrigin: true,
		pathRewrite:{
			'^/api':'/'
		}
      },
      '/test': {
        target: 'http://192.168.3.17:8080/',
        changeOrigin: true,
		pathRewrite:{
			'^/test':'/'
		}
      }
    }
  }
}